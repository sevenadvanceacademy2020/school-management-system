const express = require('express')
const cors = require('cors')
const app = express();
// const http = require('http').createServer(app);

app.use(cors());
app.get('/download', (req, res, next) => {
  res.setHeader("Access-Control-Expose-Headers", "content-Disposition")
  res.download("files/VirtualBox-6.1.6-137129-win.exe", err => {
    if(!err) console.log('download succesfully');
  })
})

app.listen(3000, () => {
  console.log("server is listening on port 3000");
})