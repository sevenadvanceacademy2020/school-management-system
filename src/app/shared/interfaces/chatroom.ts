import { User } from './user';
export interface Chatroom {
    id:string;
    name:string;
    createdBy:string;
    members:string[];
    createdOn:string;
}
