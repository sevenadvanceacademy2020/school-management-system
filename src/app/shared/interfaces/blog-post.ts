export interface BlogPost {
    sys: {
        id:string;
        type:string;
        createdAt: Date;
    };
    fields:{
        title:string;
        category:string;
        description:string;
        text:string;
        postImages: Asset[];
        image:Asset;
        createdOn:Date;
    };
    metadata: {
        tags:[]
    };
}

export interface Asset {
    fields: {
        file: {
            url:string;
            title:string;
        }
    }
    metadata: {
        tags:[]
    }
    sys: {
        id:string;
    }
}