export interface Course {
    id:string;
    name:string;
    instructor:string;
    introVideoUrl?:string;
    createdOn:any;
    createdBy:string;
    creatorId:string;
    details:string;
    school:string;
    students:string[];
    imageUrl?:string;
}

export interface Comment {
    id:string;
    commenter:string;
    date:any;
    comment:string;
}

export interface Lesson {
    id:string;
    title:string;
    createdOn:any;
    description:string;
    whatLearn?:string;
    filesUrls:string[]; 
    course:string;
    resourceLink?:string;
    createdBy:string;
}