import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { Router } from '@angular/router';
import { PopupService } from './popup.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User = JSON.parse(localStorage.getItem('userAs'))
  emptyUser:User = undefined;
  // checks if any login status has been set in the local storage
  logged: boolean = (localStorage.getItem('userAs') !==null) || false;
  isStudent: boolean = false;
  isInstructor: boolean = false;
  isAdmin: boolean = false;
  notVerified: boolean = false;
  userExists: boolean = false;
  showLoader:boolean = false;
  private usersCollection: AngularFirestoreCollection<any>;


  constructor(public auth: AngularFireAuth, private afs: AngularFirestore, private router: Router, private popupService:PopupService) {
    this.usersCollection = afs.collection<any>('users');
    this.auth.onAuthStateChanged((user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        this.getUser(user).then((doc) => {
          this.user = doc.data();
          this.logged = true;
          localStorage.setItem('userAs', JSON.stringify(this.user));
        })
        // ...    
        
        // run fuction to determine access rights  
        // saves the login status to the local storage
        
      } else {
        // User is signed out
        this.logged = false;
        // this.user = undefined
      }
    });
  }

  // gets the user information from the component and uses it to sign the user up
  emailSignup(firstName: string, lastName: string, phoneNumber: string, email: string, password: string) {
    this.auth.createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {


        // passes user info to the function which has to store the user information to the firestore database
        this.setUserData(firstName, lastName, phoneNumber, userCredential.user).then(() => {
          // ...
          // Signed in 
          this.logged = true;
          // navigates to new users page where he will await verification
          this.router.navigate(['user-profile']);
          // run fuction to determine access rights
        })
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        this.popupService.failurePopup(errorMessage)
        // ..
      });
  }

  // function to login users with email and password
  emailLogin(email: string, password: string) {
    this.auth.signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in
        // get user from firestore
        // converts function returned from getUser to a promise so it can await response before proceeding to the next steps
        this.showLoader = true
        this.getUser(userCredential.user).then((doc) => {
          this.user = doc.data();
          localStorage.setItem('userAs', JSON.stringify(this.user));
          this.logged = true;
          this.showLoader = false
          this.router.navigate(['user-profile']);
        })
        // ...

      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        this.showLoader = false
        this.popupService.failurePopup(errorMessage)
      });
  }

  // google signup fuction
  googleSignup() {
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((result) => {
        /** @type {firebase.auth.OAuthCredential} */
        var credential = result.credential;

        this.checkUserExistsInDb(result.user);

        // This gives you a Google Access Token. You can use it to access the Google API.
        // var token = credential.accessToken;
        // The signed-in user info.

      }).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        this.popupService.failurePopup(errorMessage)
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      })
  }

  // google login function
  googleLogin() {
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then((result) => {
        /** @type {firebase.auth.OAuthCredential} */
        var credential = result.credential;
        // passes user info to the function which has to store the user information to the firestore database
        this.showLoader = true;
        this.getUser(result.user).then((doc) => {
          this.user = doc.data();
          localStorage.setItem('userAs', JSON.stringify(this.user));
          this.logged = true;
          this.showLoader = false
          this.router.navigate(['user-profile']);
          // ...
        })
        // ...

      }).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        this.showLoader = false
        this.popupService.failurePopup(errorMessage)
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      })
  }

  // facebook sign up
  facebookSignup() {
    this.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((result) => {
        /** @type {firebase.auth.OAuthCredential} */
        var credential = result.credential;

        // This gives you a Google Access Token. You can use it to access the Google API.
        // var token = credential.accessToken;
        // The signed-in user info.
        // var user = result.user;
        // passes user info to the function which has to store the user information to the firestore database
        this.setUserData("", "", "", result.user);
        // ...
        this.logged = true;
        // ...
        // navigates to new users page where he will await verification
        this.router.navigate(['user-profile']);
      }).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      })
  }

  // facebook login
  facebookLogin() {
    this.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((result) => {
        /** @type {firebase.auth.OAuthCredential} */
        var credential = result.credential;

        // gets user info from database
        this.getUser(result.user).then((doc) => {
          this.user = doc.data();
          this.logged = true;
          this.router.navigate(['user-profile']);
        })
        // ...
      }).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      })
  }

  // logs out the user
  logout() {
    this.auth.signOut();
    this.router.navigate(['/auth/login']);
    localStorage.removeItem('userAs')
  }

  // sends the user information to firebase
  setUserData(firstName: string, lastName: string, phoneNumber: string, user: any) {
    // creates a variable that will hold user information before sending it to firestore
    const userData: User = {
      // gets the user id from the credential id generated by firebase
      uid: user.uid,
      email: user.email || null,
      firstName: firstName,
      lastName: lastName,
      // in case sign up with google displayname is simply displayname but is first name and last name if signup with email
      displayName: user.displayName || firstName + " " + lastName,
      phoneNumber: user.phoneNumber || phoneNumber,
      photoURL: user.photoURL || "https://picsum.photos/seed/picsum/200/200/?grayscale",
      role: {
        student: false,
        instructor: false,
        admin: false
      }
    }
    this.user = userData;
    localStorage.setItem('userAs', JSON.stringify(this.user));
    // adds the user info to firestore storing into a document with reference as user id
    return this.usersCollection.doc(userData.uid).set(userData, { merge: true });
  }

  // function to get a certain user's info from firestore 
  // function takes in user object from login
  getUser(user: any) {
    // searches firestore for user id and returns a function which is to be converted to a promise
    const userRef = this.usersCollection.doc(user.uid);
    return userRef.get().toPromise()
  }

  // check if user exists
  checkUserExistsInDb(user: any) {
    const userRef = this.usersCollection.doc(user.uid);
    userRef.get().toPromise().then((doc) => {
      if (doc.exists) {
        this.logout();
        this.popupService.failurePopup("sorry it looks like someone already exists with these credentials. You should login instead")
        this.userExists = true;
      } else {
        this.setUserData("", "", "", user)
          .then(() => {
            // ...
            this.logged = true;
            this.user = user;
            localStorage.setItem('userAs', JSON.stringify(this.user));
            // ...
            // navigates to new users page where he will await verification
            this.router.navigate(['user-profile']);
          })

      }
    })
  }

}
