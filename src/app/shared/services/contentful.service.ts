import { Injectable } from '@angular/core';
import { createClient, Entry } from 'contentful';
import { environment } from 'src/environments/environment';
import { BlogPost } from '../interfaces/blog-post';

@Injectable({
  providedIn: 'root'
})
export class ContentfulService {

  private cdaClient = createClient({
    space: environment.CONFIG.space,
    accessToken: environment.CONFIG.accessToken
  });

  constructor() { }

  getBlogs(query?: object) {
    return this.cdaClient.getEntries<BlogPost>(query)
    .then(res => res.items)
  }

  getBlog(id:any) {
    return this.cdaClient.getEntry(id)
  }
}
