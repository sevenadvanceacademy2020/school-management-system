import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { School } from '../interfaces/school';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  currentSchool!:School;
  schools: Observable<School[]>;
  private schoolsCollection: AngularFirestoreCollection<any>;

  constructor(private afs: AngularFirestore) {
    this.schoolsCollection = afs.collection<School>('schools');
    this.schools = this.schoolsCollection.valueChanges();
   }

   createSchool(school:School) {
     return this.schoolsCollection.doc(school.id).set(school, {merge:true})
   }

   getSchool(id:any) {
     return this.schoolsCollection.doc(id).get().toPromise()
   }

   getSchools() {
    return this.afs.collection<School>('schools', ref => ref
    .orderBy('name', 'asc').limit(4)).valueChanges()
   }

   verifySchoolRole(id:any, role:boolean) {
    this.schoolsCollection.doc(`${id}`).update({role:role})
   }
}
