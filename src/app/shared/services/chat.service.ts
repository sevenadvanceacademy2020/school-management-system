import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { Chatroom } from '../interfaces/chatroom';
import { SingleMessage } from '../interfaces/single-message';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  messageArray!: SingleMessage[]
  messages!: Observable<SingleMessage[]>;
  chatRooms: Observable<Chatroom[]>
  private messagesCollection: AngularFirestoreCollection<any>;
  private chatRoomCollection: AngularFirestoreCollection<any>;
  messageCollection!: AngularFirestoreCollection<SingleMessage>;

  constructor(private afs: AngularFirestore) {
    this.messagesCollection = afs.collection<SingleMessage>('messages');
    this.chatRoomCollection = afs.collection('chatRooms');
    this.chatRooms = this.afs.collection<Chatroom>('chatRooms', ref => ref.where('createdBy', '>', '')).valueChanges()
  }

  getSenderMessages(sender: string, receiver: string) {
    return this.afs.collection<SingleMessage>('messages', ref => ref.where('sender', '==', sender).where('receiver', '==', receiver)).valueChanges(['added'])
  }

  getReceiverMessages(sender: string, receiver: string) {
    return this.afs.collection<SingleMessage>('messages', ref => ref.where('sender', '==', receiver).where('receiver', '==', sender)).valueChanges(['added'])
  }

  sendMessage(message: SingleMessage) {
    this.messagesCollection.doc(message.id).set(message);
  }

  createChatRoom(chatRoom: Chatroom) {
    return this.chatRoomCollection.doc(chatRoom.id).set(chatRoom)
  }

  //  checkRoomSender(sender:string) {
  //    return this.afs.collection('chatRoom', ref => ref.where('members', 'array-contains', sender)).doc().collection('messages').valueChanges(['added'])
  //  }

  //  checkRoomReceiver(receiver:string) {
  //   return this.afs.collection('chatRoom', ref => ref.where('members', 'array-contains', receiver)).doc().collection('messages').valueChanges(['added'])
  //  }

  // myPromise = new Promise(function(resolve, reject) {

  // })

  // checkRoomSender(senderId: string, receiverId: string) {
  //   return new Promise((resolve, reject) =>{
  //     let roomRef = this.chatRoomCollection.doc(senderId + receiverId)
  //     roomRef.get().toPromise().then((doc) => {
  //       if(doc.exists) {
  //         this.messageCollection = roomRef.collection<SingleMessage>('messages')
  //       }
  //     })
  //   })

  // }

  checkRoomSender(sender: User, receiver: User) {
    let roomRef = this.chatRoomCollection.doc(sender.uid + receiver.uid)
    // roomRef.set({name:sender.displayName+ " and "+ receiver.displayName}, { merge: true })
    this.messageCollection = roomRef.collection<SingleMessage>('messages')
    return this.messageCollection.valueChanges(['added'])
  }

  checkRoomReceiver(sender: User, receiver: User) {
    let roomRef = this.chatRoomCollection.doc(receiver.uid + sender.uid)
    // roomRef.set({name:sender.displayName+ " and "+ receiver.displayName}, { merge: true })
    this.messageCollection = roomRef.collection<SingleMessage>('messages')
    return this.messageCollection.valueChanges(['added'])
  }

  checkChatRoom(id:string) {
    let roomRef = this.chatRoomCollection.doc(id)
    this.messageCollection = roomRef.collection<SingleMessage>('messages')
    return this.messageCollection.valueChanges(['added'])
  }

  // checkRoomReceiver(senderId: string, receiverId: string) {
  //   const roomRef = this.chatRoomCollection.doc(receiverId + senderId)
  //   roomRef.get().toPromise().then((doc) => {
  //     console.log("arrived second check")
  //     this.getMessages(roomRef)
  //     // if (doc.exists) {
  //     //   console.log("arrived second check")
  //     //   this.getMessages(roomRef)
  //     // } else {
  //     //   // this.createRoomDoc(senderId, receiverId)
  //     //   this.getMessages(roomRef)
  //     // }
  //   })
  // }

  // createRoomDoc(senderId: string, receiverId: string) {
  //   const roomRef = this.chatRoomCollection.doc(receiverId + senderId)
  //   roomRef.collection('messages').add({})
  //     .then(() => {
  //       this.messageCollection = roomRef.collection<SingleMessage>('messages')
  //       this.messages = this.messageCollection.valueChanges(['added'])
  //     })
  // }

  getMessages(ref: AngularFirestoreDocument) {
    this.messageCollection = ref.collection<SingleMessage>('messages')
    console.log('arrived third check')
    this.messages = this.messageCollection.valueChanges(['added'])
    // this.messageSubject.next(this.messages)
    // return this.messageCollection.valueChanges(['added']).toPromise()
  }

  sendAMessage(message: SingleMessage) {
    this.messageCollection.doc(message.id).set(message).then(() => {
    })
  }
}
