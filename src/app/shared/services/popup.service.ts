import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {


  showPopup:boolean = false;
  popupMessage:string = "";
  errorMessage:boolean = false;

  constructor() { }

  successPopup(message:string) {
    this.showPopup = true
    this.popupMessage = message
    this.errorMessage = false
  }

  failurePopup(message:string) {
    this.showPopup = true
    this.popupMessage = message
    this.errorMessage = true
  }
}
