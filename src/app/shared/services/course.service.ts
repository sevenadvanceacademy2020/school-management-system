import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Course, Comment, Lesson } from '../interfaces/course';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  currentCourse!:Course;
  currentInstructor!:User;
  courses:Observable<Course[]>
  private courseCollection:AngularFirestoreCollection<any>;
  private commentsCollection:AngularFirestoreCollection<any>;
  private lessonsCollection:AngularFirestoreCollection<any>;
  constructor(private afs: AngularFirestore) { 
    this.courseCollection = afs.collection<Course>('courses');
    this.courses = this.courseCollection.valueChanges();
    this.commentsCollection = afs.collection<Comment>('comments')
    this.lessonsCollection = afs.collection('lessons')
  }

  createCourse(course:Course) {
    return this.courseCollection.doc(course.id).set(course, {merge:true})
  }

  getSchoolCourses(schoolId:string) {
    return this.afs.collection<Course>('courses', ref => ref.where('school', '==', schoolId)).valueChanges()
  }

  getUserCourses(userEmail:string) {
    return this.afs.collection<Course>('courses', ref => ref.where('students', 'array-contains', userEmail)).valueChanges()
  }

  getCourse(id:any) {
    return this.courseCollection.doc(id).get().toPromise()
  }

  createComment(course:Course, comment:Comment) {
    let courseRef = this.courseCollection.doc(course.id)
    this.commentsCollection = courseRef.collection('comments')
    return this.commentsCollection.doc(comment.id).set(comment, {merge:true})
  }

  getComments(course:Course) {
    let courseRef = this.courseCollection.doc(course.id)
    this.commentsCollection = courseRef.collection('comments')
    return this.commentsCollection.valueChanges()
  }

  createLesson(course:Course, lesson:Lesson) {
    let courseRef = this.courseCollection.doc(course.id)
    this.lessonsCollection = courseRef.collection('lessons')
    return this.lessonsCollection.doc(lesson.id).set(lesson, {merge:true})
  }

  getLessons(course:Course) {
    let courseRef = this.courseCollection.doc(course.id)
    this.lessonsCollection = courseRef.collection('lessons')
    return this.lessonsCollection.valueChanges()
  }

  getLesson(course:Course, id:any) {
    let courseRef = this.courseCollection.doc(course.id)
    this.lessonsCollection = courseRef.collection('lessons')
    return this.lessonsCollection.doc(id).get().toPromise()
  }

  getStudents(email:string) {
    return this.afs.collection<User>('users', ref => ref.where('email', '==', email)).valueChanges()
  }

  getInstructor(course:Course) {
    return this.afs.collection<User>('users', ref => ref.where('email', '==', course.instructor)).valueChanges()
  }
}
