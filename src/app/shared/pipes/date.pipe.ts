import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dater'
})
export class DatePipe implements PipeTransform {

  transform(value: any): string {
    let date = value.slice(0, 10).replace(/-/g, "/") +
      " " +
      new Date(value).getHours() +
      ":" +
      new Date(value).getMinutes() +
      ":" +
      new Date(value).getSeconds()
    return date
  }

}
