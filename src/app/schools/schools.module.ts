import { MaterialModule } from './../shared/module/material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';

import { SchoolsRoutingModule } from './schools-routing.module';
import { SchoolsComponent } from './schools.component';
import { SchoolsDetailsComponent } from './schools-details/schools-details.component';
import { CreateSchoolComponent } from './create-school/create-school.component';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateCourseComponent } from './schools-details/create-course/create-course.component';
import { CourseDetailsComponent } from './schools-details/course-details/course-details.component';
import { DatePipe } from '../shared/pipes/date.pipe';
import { LessonDetailsComponent } from './schools-details/course-details/lesson-details/lesson-details.component';


@NgModule({
  declarations: [
    SchoolsComponent,
    SchoolsDetailsComponent,
    CreateSchoolComponent,
    CreateCourseComponent,
    CourseDetailsComponent,
    DatePipe,
    LessonDetailsComponent,
  ],
  imports: [
    CommonModule,
    SchoolsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule
  ]
})
export class SchoolsModule { }
