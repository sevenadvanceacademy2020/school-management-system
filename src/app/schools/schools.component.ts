import { SchoolService } from './../shared/services/school.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit {
  getAllSchools = this.getallSchools.schools;
  isSchools:any[] = [];
  constructor(private activeRoute:ActivatedRoute,
     private getallSchools:SchoolService) { 
  }

  ngOnInit(): void {
    this.getallSchools.schools.subscribe((schools )=> {
      this.isSchools = schools
    })
  }

   getSchool() {
    this.activeRoute.params.subscribe((param)=> {
      console.log(param);
    })
  }
    
}
