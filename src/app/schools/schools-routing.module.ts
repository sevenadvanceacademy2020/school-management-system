import { SchoolsDetailsComponent } from './schools-details/schools-details.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchoolsComponent } from './schools.component';
import { CreateSchoolComponent } from './create-school/create-school.component';
import { CreateCourseComponent } from './schools-details/create-course/create-course.component';
import { CourseDetailsComponent } from './schools-details/course-details/course-details.component';
import { LessonDetailsComponent } from './schools-details/course-details/lesson-details/lesson-details.component';
import { AuthGuard } from '../shared/guards/auth.guard';

const routes: Routes = [
  { path: '', component: SchoolsComponent },
{path:'create-school', component:CreateSchoolComponent, canActivate:[AuthGuard]},
{path:'details/:idS', component:SchoolsDetailsComponent},
{path:'details/:idS/create-course', component:CreateCourseComponent, canActivate:[AuthGuard]},
{path:'details/:idS/course/:idC', component:CourseDetailsComponent, canActivate:[AuthGuard]},
{path:'details/:idS/course/:idC/lesson/:idL' ,component:LessonDetailsComponent, canActivate:[AuthGuard]}
// children: [
//   // {path:'details', component:SchoolsDetailsComponent}
// ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolsRoutingModule { }
