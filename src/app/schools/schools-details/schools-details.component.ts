import { SchoolService } from './../../shared/services/school.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CourseService } from '../../shared/services/course.service';
import { Course } from '../../shared/interfaces/course';
import { School } from '../../shared/interfaces/school';
import { AuthService } from '../../shared/services/auth.service';
import { User } from 'src/app/shared/interfaces/user';
import { PopupService } from 'src/app/shared/services/popup.service';

@Component({
  selector: 'app-schools-details',
  templateUrl: './schools-details.component.html',
  styleUrls: ['./schools-details.component.scss'],
})
export class SchoolsDetailsComponent implements OnInit {
  courses!: Course[];
  school: School;
  paramsId: any;
  getStudent: any;
  editForm: boolean = false;
  currentCourse: Course;
  currentIndex: number = -1;
  studentEmail: string;
  studentForm: boolean = false;
  user: User;

  constructor(
    public courseService: CourseService,
    private activeRoute: ActivatedRoute,
    private getParticularSchool: SchoolService,
    private authService: AuthService,
    private popupService: PopupService
  ) { }

  ngOnInit(): void {
    this.getSchool()
    this.user = this.authService.user
  }

  getSchool() {
    this.activeRoute.params.subscribe((params) => {
      this.paramsId = params.idS;
      this.getParticularSchool.getSchool(this.paramsId).then((doc) => {
        this.school = doc.data()
        this.getParticularSchool.currentSchool = this.school
        this.getCourses()
      })
    });
  }

  getCourses() {
    this.courseService.getSchoolCourses(this.school.id).subscribe((courses) => {
      this.courses = courses
      console.log(courses)
    })
  }

  editCourse(course: Course, i: number) {
    this.editForm = !this.editForm
    this.currentIndex = i
    this.currentCourse = course
  }

  saveCourse() {
    this.courseService.createCourse(this.currentCourse).then(() => {
      this.popupService.successPopup("course successfully modified")
      this.editForm = false
    })
  }

  student(course: Course) {
    this.currentCourse = course
    this.studentForm = !this.studentForm
  }

  addStudent() {
    if (this.currentCourse.students.includes(this.studentEmail)) {
      this.popupService.failurePopup("student already member of course")
    } else {
      this.courseService.getStudents(this.studentEmail).subscribe((student) => {
        if (student.length > 0) {
          this.currentCourse.students.push(this.studentEmail)
          this.courseService.createCourse(this.currentCourse).then(() => {
            this.popupService.successPopup("student added")
            this.studentForm = false;
          })
        } else {
          this.popupService.failurePopup("no student exists with this email")
          this.studentEmail = ''
          this.studentForm = false;
        }
      })
    }
  }

  checkAdmin(): boolean {
    if (this.school.creatorId == this.user.uid) {
      return true
    } else {
      return false
    }
  }
}
