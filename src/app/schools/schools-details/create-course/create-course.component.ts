import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { CourseService } from '../../../shared/services/course.service';
import { User } from '../../../shared/interfaces/user';
import { AuthService } from '../../../shared/services/auth.service';
import { Course } from 'src/app/shared/interfaces/course';
import firebase from 'firebase/app'
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SchoolService } from 'src/app/shared/services/school.service';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ActivatedRoute } from '@angular/router';
import { School } from '../../../shared/interfaces/school';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.scss']
})
export class CreateCourseComponent implements OnInit {

  user!: User;
  id: string;
  uploadPercent!: Observable<number | undefined>;
  // initialises an undefined string variable for later to be equated to the image url after adding product is completed
  downloadUrl!: string;
  imageUrl: string = '';
  currentSchool:School;

  courseForm = new FormGroup({
    name: new FormControl(''),
    instructor: new FormControl('', Validators.email),
    introVideoUrl: new FormControl(''),
    details: new FormControl('')
  })

  constructor(
    private storage: AngularFireStorage,
    private afs: AngularFirestore,
    public courseService: CourseService,
    private authService: AuthService,
    private schoolService: SchoolService,
    private popupService: PopupService,
    private route:ActivatedRoute
  ) {
    this.id = this.afs.createId();
  }

  ngOnInit(): void {
    this.getSchool()
    this.user = this.authService.user
  }

  get instructor() { return this.courseForm.get('instructor'); }
  get name() { return this.courseForm.get('name'); }
  get details() { return this.courseForm.get('details'); }

  createCourse() {
    console.log(this.courseForm.errors)
    const course: Course = {
      id: this.id,
      name: this.courseForm.value.name,
      instructor: this.courseForm.value.instructor,
      createdOn: firebase.firestore.Timestamp.now().toDate().toJSON(),
      createdBy: this.user.displayName,
      creatorId: this.user.uid,
      details: this.courseForm.value.details,
      school: this.currentSchool.id,
      introVideoUrl: this.courseForm.value.introVideoUrl,
      students: [],
      imageUrl: this.imageUrl
    }
    this.courseService.createCourse(course)
    .then(() => {
      this.popupService.successPopup("course successfully created")
      this.imageUrl = this.downloadUrl
      this.id = this.afs.createId();
      this.courseForm.setValue({
        name: '',
        instructor: '',
        introVideoUrl: '',
        details: ''
      })
    })
    .catch((err) => {
      this.popupService.failurePopup("something wrong happened. Please try again")
    })
  }

  getSchool() {
    const idSchool = this.route.snapshot.paramMap.get('idS')
    this.schoolService.getSchool(idSchool).then((doc) => {
      this.currentSchool = doc.data()
    })
    .catch((err) => {
      this.popupService.failurePopup("something wrong happened when getting the current school")
    })
  }

  uploadPicture(event: any) {
    const file = event.target.files[0];
    const filePath = 'courses/' + this.id;
    const fileRef = this.storage.ref(filePath);
    // uploads the chosen image to the file reference
    const task = this.storage.upload(filePath, file);
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe(downloadURL => {
          //  sets the image url to the download url gotten
          this.imageUrl = downloadURL;
          //  console.log(downloadURL);
        })
      })
    )
      .subscribe()
  }
}
