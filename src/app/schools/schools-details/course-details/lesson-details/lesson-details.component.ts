import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { SingleMessage } from 'src/app/shared/interfaces/single-message';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ChatService } from 'src/app/shared/services/chat.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { Course, Lesson } from '../../../../shared/interfaces/course';
import { User } from '../../../../shared/interfaces/user';
import firebase from 'firebase/app';
import { FormControl, FormGroup } from '@angular/forms';
import { Chatroom } from 'src/app/shared/interfaces/chatroom';

@Component({
  selector: 'app-lesson-details',
  templateUrl: './lesson-details.component.html',
  styleUrls: ['./lesson-details.component.scss']
})
export class LessonDetailsComponent implements OnInit {

  lesson!: Lesson;
  user!: User;
  course!: Course;
  instructor!: User;
  students: User[] = []
  currentChatter!: User;
  currentIndex!: number;
  currentRoomIndex!: number;
  chatRooms!: Chatroom[]
  currentChatterName!: string;
  messages!: SingleMessage[];
  messageLoader: boolean = false;

  chatForm = new FormGroup({
    content: new FormControl('')
  });

  constructor(
    private route: ActivatedRoute,
    public courseService: CourseService,
    public authService: AuthService,
    private afs: AngularFirestore,
    public chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.user = this.authService.user
    this.getLesson()
  }

  getLesson() {
    const idCourse = this.route.snapshot.paramMap.get('idC')
    this.courseService.getCourse(idCourse).then((doc) => {
      this.course = doc.data()
      const idLesson = this.route.snapshot.paramMap.get('idL')
      this.courseService.getLesson(this.course, idLesson).then((doc) => {
        this.lesson = doc.data()
        this.getInstructor()
        this.getStudents()
      })
    })
  }

  getInstructor() {
    this.courseService.getInstructor(this.course).subscribe((instructors) => {
      this.instructor = instructors[0]
    })
  }

  getStudents() {
    this.course.students.forEach((student) => {
      this.courseService.getStudents(student).subscribe((student) => {
        this.students.push(student[0])
      })
    })
  }


  setCurrent(student: User, i: number) {
    // sets the currently active student
    // current student who will be the recipient of the messages
    this.currentChatter = student;
    // equates an index which is used for styling of the student
    this.currentIndex = i;
    console.log(this.currentChatter)
    this.messages = []
    this.messageLoader = true

    this.chatService.checkRoomSender(this.user, this.currentChatter).subscribe((messages) => {
      const m1 = messages
      this.chatService.checkRoomReceiver(this.user, this.currentChatter).subscribe((messages) => {
        const m2 = messages
        const finalResults = [...m1, ...m2]
        console.log(finalResults)
        this.messageLoader = false
        this.messages = this.transformMessages(finalResults)
      })
    })
  }

  setCurrentRoom(chatroom: Chatroom, index: number) {
    this.currentChatterName = "";
    this.currentIndex = -1
    this.currentRoomIndex = index
    this.messageLoader = true
    this.chatService.checkChatRoom(chatroom.id).subscribe((messages) => {
      this.messageLoader = false
      this.messages = this.transformMessages(messages)
    })
  }



  sendAMessage() {
    const sender = this.user.displayName;
    const content = this.chatForm.value.content;
    const id = this.afs.createId();
    const receiver = this.currentChatter.displayName;
    const newMessage: SingleMessage = {
      id: id,
      sender: sender,
      receiver: receiver,
      content: content,
      sentOn: firebase.firestore.Timestamp.now().toDate().toJSON()
    };
    this.chatService.sendAMessage(newMessage);
    this.chatForm = new FormGroup({
      content: new FormControl('')
    });
  }

  transformMessages(messages: SingleMessage[]) {
    return messages.sort(function (x, y) {
      let a = new Date(x.sentOn).getTime(),
        b = new Date(y.sentOn).getTime()
      return a == b ? 0 : a > b ? 1 : -1;
    })
      .map((message) => {
        message = {
          ...message, sentOn: message.sentOn.slice(0, 10).replace(/-/g, "/") +
            " " +
            new Date(message.sentOn).getHours() +
            ":" +
            new Date(message.sentOn).getMinutes() +
            ":" +
            new Date(message.sentOn).getSeconds()
        }
        return message
      })
  }


}
