import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course, Lesson, Comment } from 'src/app/shared/interfaces/course';
import { User } from 'src/app/shared/interfaces/user';
import { CourseService } from '../../../shared/services/course.service';
import { AuthService } from '../../../shared/services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app'
import { finalize } from 'rxjs/operators';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormGroup, FormControl } from '@angular/forms';
import { ChatService } from 'src/app/shared/services/chat.service';
// import { DatePipe } from 'src/app/shared/pipes/date.pipe';
import { Chatroom } from '../../../shared/interfaces/chatroom';
import { PopupService } from 'src/app/shared/services/popup.service';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss']
})
export class CourseDetailsComponent implements OnInit {

  course!: Course;
  showLessonForm: boolean = false;
  lessonsNumber: number = 0;
  lessons!: Lesson[];
  students: User[] = []
  instructor!: User;
  user!: User;
  isStudent: boolean = false;
  isInstructor: boolean = false;
  commentText!: string;
  comments!: Comment[]

  filesUrl: string[] = []

  lessonForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    whatLearn: new FormControl(''),
    resourceLink: new FormControl('')
  })

  constructor(
    private route: ActivatedRoute,
    public courseService: CourseService,
    public authService: AuthService,
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
    private popupService: PopupService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.user = this.authService.user
    this.getCourse()
  }

  getCourse() {
    const idCourse = this.route.snapshot.paramMap.get('idC');
    this.courseService.getCourse(idCourse).then((doc) => {
      this.course = doc.data()
      this.courseService.currentCourse = this.course
      this.getStudents()
      this.getComments(this.course)
      this.getLessons()
      this.getInstructor()
      this.isStudent = this.course.students.includes(this.user.email)
      this.isInstructor = this.checkInstructor()
      if (!this.isStudent) {
        console.log("not student")
        if (!this.isInstructor) {
          this.popupService.failurePopup("you are not a member of this course")
          this.router.navigate(['/schools'])
        }
      }
    })
  }


  getStudents() {
    this.course.students.forEach((student) => {
      this.courseService.getStudents(student).subscribe((student) => {
        this.students.push(student[0])
        console.log(this.students)
      })
    })
  }

  getInstructor() {
    this.courseService.getInstructor(this.course).subscribe((instructors) => {
      this.instructor = instructors[0]
      this.courseService.currentInstructor = this.instructor
      console.log(this.instructor)
    })
  }

  checkInstructor(): boolean {
    if (this.course.instructor == this.user.email) {
      return true
    } else {
      return false
    }
  }

  getAddedStudent(student: string) {
    this.courseService.getStudents(student).subscribe((student) => {
      this.students.push(student[0])
    })
  }

  addComment() {
    const newComment: Comment = {
      id: this.afs.createId(),
      commenter: this.user.displayName,
      comment: this.commentText,
      date: firebase.firestore.Timestamp.now().toDate().toJSON()
    }
    this.courseService.createComment(this.course, newComment).then(() => {
      this.popupService.successPopup("your comment has been added")
      this.commentText = ''
    })
  }

  getComments(course: Course) {
    this.courseService.getComments(course).subscribe((comments) => {
      this.comments = comments
      console.log(this.comments)
    })
  }

  createLesson() {
    const newLesson: Lesson = {
      id: this.afs.createId(),
      title: this.lessonForm.value.title,
      description: this.lessonForm.value.description,
      resourceLink: this.lessonForm.value.resourceLink,
      course: this.course.id,
      filesUrls: this.filesUrl,
      createdBy: this.user.displayName,
      createdOn: firebase.firestore.Timestamp.now().toDate(),
      whatLearn: this.lessonForm.value.whatLearn
    };
    // const newChatRoom: Chatroom = {
    //   name: newLesson.title + ' chatroom',
    //   id: newLesson.id,
    //   createdBy: this.user.displayName,
    //   createdOn: firebase.firestore.Timestamp.now().toDate().toJSON(),
    //   members: []
    // }
    this.courseService.createLesson(this.course, newLesson).then(() => {
      this.popupService.successPopup("new lesson created")
      this.showLessonForm = false;
    })
  }

  getLessons() {
    this.courseService.getLessons(this.course).subscribe((lessons) => {
      this.lessons = lessons;
      this.lessonsNumber = lessons.length
    })
  }

  uploadPictures(event: any) {
    const fileList = event.target.files
    for (const file of fileList) {
      // const file = event.target.files[i];
      const filePath = 'lessons/' + this.course.id + '/' + this.afs.createId() + Date.now().toString();
      const fileRef = this.storage.ref(filePath);
      // uploads the chosen image to the file reference
      const task = this.storage.upload(filePath, file);
      // observe percentage changes
      // this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(downloadURL => {
            //  sets the image url to the download url gotten
            this.filesUrl.push(downloadURL);
            //  console.log(downloadURL);
          })
        })
      )
        .subscribe()
    }
  }
}
