import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireStorageReference } from '@angular/fire/storage';
import firebase from 'firebase/app'
import { FormGroup, FormControl } from '@angular/forms';
import { SchoolService } from '../../shared/services/school.service';
import { School } from '../../shared/interfaces/school';
import { User } from '../../shared/interfaces/user';
import { AuthService } from '../../shared/services/auth.service';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { MaterialModule } from 'src/app/shared/module/material/material.module';
import { PopupService } from 'src/app/shared/services/popup.service';

@Component({
  selector: 'app-create-school',
  templateUrl: './create-school.component.html',
  styleUrls: ['./create-school.component.scss']
})
export class CreateSchoolComponent implements OnInit {

  id: string;
  user!: User;
  // upload percent which will be used on the progress bar
  uploadPercent!: Observable<number | undefined>;
  // initialises an undefined string variable for later to be equated to the image url after adding product is completed
  downloadUrl!: string;
  imageUrl!: string;
  imagesUrl: string[] = [];

  schoolForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    description: new FormControl(''),
    location: new FormControl(''),
    principal: new FormControl(''),
    website: new FormControl(''),
    social1: new FormControl(''),
    social2: new FormControl(''),
    social3: new FormControl(''),
    tel: new FormControl(''),
    registration: new FormControl('')
  })

  constructor(
    private storage: AngularFireStorage,
    private afs: AngularFirestore,
    public schoolService: SchoolService,
    public authService: AuthService,
    public material: MaterialModule,
    private popupService: PopupService
  ) {
    this.id = this.afs.createId();
  }

  ngOnInit(): void {
    this.user = this.authService.user
  }

  get schoolName() { return this.schoolForm.get('name'); }
  get schoolLocation() { return this.schoolForm.get('location'); }
  get schoolPrincipal() { return this.schoolForm.get('principal'); }
  get schoolTel() { return this.schoolForm.get('tel'); }
  get schoolEmail() { return this.schoolForm.get('email'); }
  get schoolDescription() { return this.schoolForm.get('description'); }
  get schoolReg() { return this.schoolForm.get('registration'); }

  createSchool() {
    const school: School = {
      id: this.id,
      name: this.schoolForm.value.name,
      email: this.schoolForm.value.email,
      description: this.schoolForm.value.description,
      location: this.schoolForm.value.location,
      createdBy: this.user.displayName,
      creatorId: this.user.uid,
      createdOn: firebase.firestore.Timestamp.now().toDate().toJSON(),
      principalName: this.schoolForm.value.principal,
      pictureUrl: this.imageUrl,
      phoneNumber: this.schoolForm.value.tel,
      regNumber: this.schoolForm.value.registration,
      picturesUrls: this.imagesUrl,
      websiteUrl: this.schoolForm.value.website,
      socialLinks: [this.schoolForm.value.social1, this.schoolForm.value.social2, this.schoolForm.value.social3],
      isVerified: false,
      students: []
    }
    this.schoolService.createSchool(school).then(() => {
      this.popupService.successPopup("school created")
      this.schoolForm.setValue({
        name: '',
        email: '',
        description: '',
        location: '',
        principal: '',
        website: '',
        social1: '',
        social2: '',
        social3: '',
        tel: '',
        registration: ''
      })
      this.imageUrl = this.downloadUrl
    }).catch((err) => {
      this.popupService.failurePopup(err.message + " something went wrong")
    })
    this.id = this.afs.createId();
  }

  uploadPicture(event: any) {
    const file = event.target.files[0];
    const filePath = 'schools/schoolProfile/' + this.id;
    const fileRef = this.storage.ref(filePath);
    // uploads the chosen image to the file reference
    const task = this.storage.upload(filePath, file);
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe(downloadURL => {
          //  sets the image url to the download url gotten
          this.imageUrl = downloadURL;
          //  console.log(downloadURL);
        })
      })
    )
      .subscribe()
  }

  uploadPictures(event: any) {
    const fileList = event.target.files
    for (const file of fileList) {
      // const file = event.target.files[i];
      const filePath = 'schools/schoolimages/' + this.id + Date.now().toString();
      const fileRef = this.storage.ref(filePath);
      // uploads the chosen image to the file reference
      const task = this.storage.upload(filePath, file);
      // observe percentage changes
      // this.uploadPercent = task.percentageChanges();
      // get notified when the download URL is available
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(downloadURL => {
            //  sets the image url to the download url gotten
            this.imagesUrl.push(downloadURL);
            //  console.log(downloadURL);
          })
        })
      )
        .subscribe()
    }
  }

}
