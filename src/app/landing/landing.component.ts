import { Component, OnInit } from '@angular/core';
import { SchoolService } from '../shared/services/school.service';
import { School } from '../shared/interfaces/school';
import { ContentfulService } from '../shared/services/contentful.service';
import { Entry } from 'contentful';
import { BlogPost } from '../shared/interfaces/blog-post';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  schools!:School[];
  blogs!: Entry<any>[];

  constructor(
    public schoolService:SchoolService,
    public contentFulService:ContentfulService
  ) { }

  ngOnInit(): void {
    this.getSchools()
    this.getBlogs()
  }

  getSchools() {
    this.schoolService.getSchools().subscribe((schools) => {
      this.schools = schools
    })
  }

  getBlogs() {
    this.contentFulService.getBlogs({include: 3})
    .then((blogs) => {
      this.blogs = blogs
      console.log(this.blogs)
    })
  }

}



