// import { MatNativeDateModule } from '@angular/material/native-date';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from "@angular/material/button";
import { HighchartsChartModule } from 'highcharts-angular';

import { AngularFireModule } from '@angular/fire';
import { PERSISTENCE } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { HeaderComponent } from './partial/header/header.component';
// import {MatNativeDateModule} from '@angular/material';
// import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {MatSliderModule} from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatCarouselModule } from 'ng-mat-carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './partial/footer/footer.component';
import { LandingComponent } from './landing/landing.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { DatePipe } from '@angular/common';
import { PopupComponent } from './partial/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,   
    HeaderComponent, FooterComponent, LandingComponent, PopupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    BrowserAnimationsModule,
    MatButtonModule,
    HighchartsChartModule,
    MatSliderModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MatToolbarModule,
    MatNativeDateModule,
    // MatMomentDateModule,
    FlexLayoutModule,
    FontAwesomeModule,
    MatCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  providers: [
    // { provide: PERSISTENCE, useValue: 'session' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
