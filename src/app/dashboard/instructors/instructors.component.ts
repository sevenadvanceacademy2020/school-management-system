import { GeneralService } from './../services/general.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/interfaces/user';
import { UsersService } from 'src/app/shared/services/users.service';

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.scss']
})
export class InstructorsComponent implements OnInit {
  list: boolean = false;
  allInstructors = this.general.userArray;
  instructors!: User[];
  constructor(private general: GeneralService, private userService: UsersService) { }

  ngOnInit(): void {
    this.userService.getInstructors().subscribe((instructors) => {
      this.instructors = instructors
    })
  }
  toggleDefault() {
    this.list = false;
  }
  toggleList() {
    this.list = true;
  }


}
