import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { switchMap, map, first } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  initPeer(): any {
    throw new Error('Method not implemented.');
  }
  userArray!: Observable<any[]>;
  instructorsArray!: Observable<any[]>;
  private collection!: AngularFirestoreCollection<any>;
  remoteStreamBs: any;
  mediaCall: any;
  isCallStartedBs: any;
  localStreamBs: any;
  peer: any;
  snackBar: any;
  constructor(
    private readonly fs: AngularFirestore,
    private afAuth: AngularFireAuth,
  ) {
    this.collection = fs.collection(`users`);
    this.getAllUsers();

  }

  public async establichMediaCall(remotePeerId: string) {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true,
      });

      const connection = this.peer.connect(remotePeerId);
      connection.on('error', (err: any) => {
        console.error(err);
        this.snackBar.open(err, 'Close');
      });
   
      connection.on('open', () =>{
        console.log('hi');
        
      })
      this.mediaCall = this.peer.call(remotePeerId, stream);

      if (!this.mediaCall) {
        let errorMessage = 'Unable to connect to remote peer';
        this.snackBar.open(errorMessage, 'Close');
        throw new Error(errorMessage);
      }

      this.localStreamBs.next(stream);
      this.isCallStartedBs.next(true);

      this.mediaCall.on('stream', (remoteStream: MediaStream) => {
        this.remoteStreamBs.next(remoteStream);
      });
   
      this.mediaCall.on('error', (err: string) => {
        this.snackBar.open(err, 'Close');
        console.log(err);
        this.isCallStartedBs.next(false);
      });
      this.mediaCall.on('close', ()=> this.onCallClose())
    } catch (error) {
      console.error(error);
      this.snackBar.open(error, 'Close');
      this.isCallStartedBs.next(false);
    }
  }

  public async enableCallAnswer() {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true,
      });
      this.localStreamBs.next(stream);
      this.peer.on('call', async (call: any) => {
        this.mediaCall = call;
        this.isCallStartedBs.next(true);
   
        this.mediaCall.answer(stream);
        this.mediaCall.on('stream', (remoteStream: MediaStream) => {
          this.remoteStreamBs.next(remoteStream);
        });
        this.mediaCall.on('error', (err: string) => {
          this.snackBar.open(err, 'Close');
          this.isCallStartedBs.next(false);
          console.error(err);
        });
        this.mediaCall.on('close', () => this.onCallClose())
      });
    } catch (error) {
      console.log(error);
      this.snackBar.open(error, 'Close');
      this.isCallStartedBs.next(false);
    }
  }

  private onCallClose() {
    this.remoteStreamBs?.value.getTracks().forEach((track: { stop: () => void; }) => {
      track.stop();
    });
    this.localStreamBs?.value.getTracks().forEach((track: { stop: () => void; }) => {
      track.stop();
    });
    this.snackBar.open('Call Ended', 'Close');
  }

  public closeMediaCall() {
    this.mediaCall?.close();
    if (!this.mediaCall) {
      this.onCallClose();
    }
    this.isCallStartedBs.next(false);
  }

  public destroyPeer() {
    this.mediaCall?.close();
    this.peer?.disconnect();
    this.peer?.destroy();
  }

  getAllUsers() {
    this.userArray = this.collection.snapshotChanges().pipe(
      map((actions) => actions.map((a) => a.payload.doc.data() as any))
    );
  }

  instructors() {
    this.instructorsArray = this.collection.snapshotChanges().pipe(
      map((actions) => actions.map((a) => a.payload.doc.data() as any))
    )
  }

  updateUserRole(id: string, user: any) {
    return this.collection.doc(id).set(user, { merge: true })
    // return new Promise(async (resolve: any, reject: any) => {
    //   let final: any;
    //   this.userArray.subscribe((arg) => arg.map((i) => {
    //     final = i.role;
    //     final = { student: newrole, }
    //   }
    //   ))
    //   // const result = this.collection.doc(id).update();

    // })

  }
}
