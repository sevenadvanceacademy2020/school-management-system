import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsNotesComponent } from './instructors-notes.component';

describe('InstructorsNotesComponent', () => {
  let component: InstructorsNotesComponent;
  let fixture: ComponentFixture<InstructorsNotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstructorsNotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
