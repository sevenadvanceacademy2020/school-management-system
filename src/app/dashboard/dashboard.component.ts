import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  ngOnInit(): void {
  }

  panelOpenState:boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}
  menuItems:any = [
    {
      title: 'dashboard',
      subtitle: 'recent analytics',
      other:[
        {icon: 'view_agenda', items:'General', link: ""},
      {icon: 'business_center', items:'Students', link: "students"},
      {icon: 'cast_for_education', items:'Instructors', link: "instructors"}
      ],
    },
    {
      title: 'applications',
      subtitle: 'current events',
      other:[
      {icon: 'school', items:'academy', link:"academy"},
      {icon: 'today', items:'calendar', link:"filemanager"},
      {icon: 'forum', items:'chat',link:"chat"},
      {icon: 'call', items:'contact', link:"contact"},
  
      ],
    },
]

pages:any = [
  {
    title: 'features and more',
    subtitle: 'activities, notes, others....',
    other:[
    {icon: 'notes', items:'Activities', link:"activity"},
    // {icon: 'email', items:'mailbox'},
    // {icon: 'text_snippet', items:'notes'},
    {icon: 'sensor_window', items:'scrumboard'},
    {icon: 'task_alt', items:'tasks', link:"task"},
    {icon: 'settings', items:'settings', link:'settings'},
    {icon: 'account_circle', items:'profile',  link:'profile'}
    ],
  },

]

}
