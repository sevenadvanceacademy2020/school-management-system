import { Component, OnInit,ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {MatAccordion} from '@angular/material/expansion';
@Component({
  selector: 'app-school-data',
  templateUrl: './school-data.component.html',
  styleUrls: ['./school-data.component.scss']
})
export class SchoolDataComponent implements OnInit {
  selectedDate: any;
  isVissible:boolean = false;
  @ViewChild(MatAccordion) accordion: MatAccordion;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  constructor(private breakpointObserver: BreakpointObserver) { 
  }

  onSelect(event:any){
    console.log(event);
    this.selectedDate= event;
  }
  ngOnInit(): void {
  }

}
