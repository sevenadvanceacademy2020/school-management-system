import { DialogData, DialogComponent } from './../shared/dialog/dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from './../services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef,OnDestroy } from '@angular/core';
import { filter, switchMap } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit,OnDestroy {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );


   isCallStarted$!:Observable<boolean>
  // private localStream!:MediaSteam;
  private peerId:any;

  @ViewChild('local_video') localVideo!:ElementRef<HTMLVideoElement>;
  @ViewChild('remote_video') remoteVideo!:ElementRef<HTMLVideoElement>;
  constructor(private dialog:MatDialog, private callService:GeneralService,
    private breakpointObserver: BreakpointObserver) { 
    this.isCallStarted$ = this.callService.isCallStartedBs;
    this.peerId = this.callService.initPeer();
  }

  ngOnInit(): void {
    this.callService.localStreamBs.pipe(filter(res => !!res)).subscribe((stream: MediaProvider) => this.localVideo.nativeElement.srcObject = stream)

    this.callService.remoteStreamBs
    .pipe(filter(res => !!res))
    .subscribe((stream: MediaProvider) => this.remoteVideo.nativeElement.srcObject = stream)
  }
  
  ngAfterViewInit(): void {
    // this.requestMediaDevices()
  }

  ngOnDestroy(): void {
    this.callService.destroyPeer();
  }

  public showModal(joinCall:boolean):void{
    let dialogData:any = joinCall ? ({peerId:null, joinCall:true}): ({ peerId: this.peerId, joinCall: false })
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data:  dialogData
    })
    dialogRef.afterClosed().pipe(
      switchMap(peerId => joinCall ? of(this.callService.establichMediaCall(peerId)) :  of(this.callService.enableCallAnswer()))
    ).subscribe(_ => {})
  }
 
 /**
  * endCall
  */
 public endCall() {
this.callService.closeMediaCall()
 }
  async requestMediaDevices():Promise<void>{
   
  }

}
