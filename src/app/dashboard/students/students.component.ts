import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { User } from 'src/app/shared/interfaces/user';
import { UsersService } from 'src/app/shared/services/users.service';
// import  {StudentData}  from "./student.ts";


const student_data: any[] = [
  {
    reg_id: 1,
    course_name: 'cybersecurity',
    matriculation_Id: '123949059',
    first_name: 'John',
    last_name: 'Doe',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 2,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'Glee',
    last_name: 'Albert',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 3,
    course_name: 'fullstack web development',
    matriculation_Id: '443945059',
    first_name: 'choupoueng',
    last_name: 'cindy',
    phone_number: '+237670798725',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 4,
    course_name: 'fullstack',
    matriculation_Id: '234904905',
    first_name: 'egbe',
    last_name: 'Alika',
    phone_number: '+237676350354',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 5,
    course_name: 'cybersecurity',
    matriculation_Id: '789949059',
    first_name: 'Ngong',
    last_name: 'Sherifa',
    phone_number: '+237679467956',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 6,
    course_name: 'fullstack',
    matriculation_Id: '211904905',
    first_name: 'Nyamsi',
    last_name: 'Danold',
    phone_number: '+2376799340820',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 7,
    course_name: 'cybersecurity',
    matriculation_Id: '673949059',
    first_name: 'Leoga',
    last_name: 'Mibel',
    phone_number: '+237670547672',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 8,
    course_name: 'fullstack',
    matriculation_Id: '678904905',
    first_name: 'Makany',
    last_name: 'Michel',
    phone_number: '+237694504420',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 9,
    course_name: 'cybersecurity',
    matriculation_Id: '123949059',
    first_name: 'Ozagi',
    last_name: 'David',
    phone_number: '+237669665029',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 10,
    course_name: 'Digital Marketing',
    matriculation_Id: '582904905',
    first_name: 'Eyong',
    last_name: 'Ako',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 11,
    course_name: 'cybersecurity',
    matriculation_Id: '789949059',
    first_name: 'John',
    last_name: 'Harry',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 12,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'uchenna',
    last_name: 'David',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 13,
    course_name: 'cybersecurity',
    matriculation_Id: '123949059',
    first_name: 'Jam',
    last_name: 'stanley',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 14,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'etah',
    last_name: 'innocent',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 15,
    course_name: 'Digital marketing',
    matriculation_Id: '123949059',
    first_name: 'samjella',
    last_name: 'angello',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 16,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'chah',
    last_name: 'hannah',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 17,
    course_name: 'cybersecurity',
    matriculation_Id: '123949059',
    first_name: 'evelyn',
    last_name: 'akono',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 18,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'joel',
    last_name: 'eder',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 19,
    course_name: 'digital marketing',
    matriculation_Id: '123949059',
    first_name: 'claudeth',
    last_name: 'ngah',
    phone_number: '+2348937485794',
    photo_url: '../../../assets/images.png',
  },
  {
    reg_id: 20,
    course_name: 'fullstack',
    matriculation_Id: '582904905',
    first_name: 'kandem',
    last_name: 'idriss',
    phone_number: '+23767485794',
    photo_url: '../../../assets/images.png',
  },
]

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  popup: boolean = false;
  data: any;
  students!: User[];

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.userService.getStudents().subscribe((u) => {
      return this.students = u
    })
    console.log(this.students);

  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit() {
    this.studentDataSource.paginator = this.paginator;
  }

  // student fields
  studentDataSource = new MatTableDataSource(this.students);
  studentColumns: string[] = [
    'photo_url', 'first_name', 'last_name', 'phone_num', 'reg_id', 'course_name', 'more'
  ];
  // get all students details
  getStudentDetails(f: any): void {
    this.data = this.students.filter((i) => i.uid = f)
    this.popup = !this.popup;
  }
  closePopup(): void {
    this.popup = false;
  }

  // filter students by search
  filterStudents(ev: Event) {
    const studentValue = (ev.target as HTMLInputElement).value;
    this.studentDataSource.filter = studentValue.trim().toLowerCase();
  }
  list: boolean = false;
  toggleDefault() {
    this.list = false;
  }
  toggleList() {
    this.list = true;
  }

}
