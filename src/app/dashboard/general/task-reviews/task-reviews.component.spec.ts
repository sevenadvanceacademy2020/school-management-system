import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskReviewsComponent } from './task-reviews.component';

describe('TaskReviewsComponent', () => {
  let component: TaskReviewsComponent;
  let fixture: ComponentFixture<TaskReviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskReviewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
