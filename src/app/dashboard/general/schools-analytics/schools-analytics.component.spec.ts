import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolsAnalyticsComponent } from './schools-analytics.component';

describe('SchoolsAnalyticsComponent', () => {
  let component: SchoolsAnalyticsComponent;
  let fixture: ComponentFixture<SchoolsAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchoolsAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolsAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
