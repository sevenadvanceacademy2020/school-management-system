import { SchoolService } from './../../../shared/services/school.service';
import { Roles, User } from './../../../shared/interfaces/user';
import { GeneralService } from './../../services/general.service';
import { Component, OnInit } from '@angular/core';

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-students-analytics',
  templateUrl: './students-analytics.component.html',
  styleUrls: ['./students-analytics.component.scss'],
})
export class StudentsAnalyticsComponent implements OnInit {
  selectedValue: any;

  roles: Food[] = [
    { value: 'admin', viewValue: 'admin' },
    { value: 'student', viewValue: 'student' },
    { value: 'instructor', viewValue: 'instructor' },
  ];
  panelOpenState: boolean = false;
  user: any = [];
  all: any;
  Mrole!: Roles;
  UserS!: User;
  users = this.allUser.userArray;
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  disabled = false;
  getAllSchool = this.getAllSchools.schools;
  constructor(private allUser: GeneralService, private getAllSchools:SchoolService) { }

  onCheck() { }

  ngOnInit(): void {
    this.all = this.users.subscribe((arg) =>
      arg.map((i) => {
        return this.user.push(i);
      })
    );
    console.log(this.user);
  }

  onUpdateUser(id: any, data: any) {
    this.allUser.updateUserRole(id, data).then(() => {
      window.alert("user role successfully updated")
    })
  }
  verifySchool(id:any, role:boolean) {
    this.getAllSchools.verifySchoolRole(id, role)
  }
 
}
