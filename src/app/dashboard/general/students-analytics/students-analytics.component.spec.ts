import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsAnalyticsComponent } from './students-analytics.component';

describe('StudentsAnalyticsComponent', () => {
  let component: StudentsAnalyticsComponent;
  let fixture: ComponentFixture<StudentsAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentsAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
