import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  data:any = [{
    name: 'first',
    data: [500, 700, 555, 444, 777, 877, 944, 567, 666, 789, 456, 654]
 }
 
,{
   name: 'last',
   data: [600, 650, 670, 700, 780, 888, 890, 900, 920, 970, 990, 1000],
},

];

 highcharts = Highcharts

 chartOptions = {   
  chart: {
     type: "spline"
  },
 
  xAxis:{
     categories:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
  },
  yAxis: {          
     title:{
        text:"Rate"
     } 
  },
  credits:{
    enabled: false
  },
  series: this.data
};

}
