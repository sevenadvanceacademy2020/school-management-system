import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.scss']
})
export class RightComponent implements OnInit {
ngOnInit(): void {}

  constructor() {}
  
  rightSection: any = [
    {
      heading: `right section`,
      upperSection: [
        {
          heading: `345`,
          sub: `enrolment`,
          bg: 'rgba(137, 137, 235, 0.109)',
          color: 'blue',
        },
        {
          heading: `305`,
          sub: `register`,
          bg: 'rgba(60, 255, 0, 0.123)',
          color: 'green',
        },
      ],
      lowerSection: [
        {
          heading: `33`,
          bg: 'rgba(60, 255, 0, 0.123)',
          color: 'rgb(123, 231, 231)',
          sub: `comment`,
        },
        {
          heading: `05`,
          bg: 'rgba(60, 255, 0, 0.123)',
          color: 'rgb(35, 76, 153)',
          sub: `news`,
        },
        {
          heading: `45`,
          bg: 'rgba(60, 255, 0, 0.123)',
          color: 'rgb(83, 233, 57)',
          sub: `other`,
        },
        {
          heading: `35`,
          bg: 'rgba(60, 255, 0, 0.123)',
          color: 'rgb(233, 57, 57)',
          sub: `fix`,
        },
      ],
    },
  ];

}
