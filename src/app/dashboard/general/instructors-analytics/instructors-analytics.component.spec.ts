import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsAnalyticsComponent } from './instructors-analytics.component';

describe('InstructorsAnalyticsComponent', () => {
  let component: InstructorsAnalyticsComponent;
  let fixture: ComponentFixture<InstructorsAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InstructorsAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
