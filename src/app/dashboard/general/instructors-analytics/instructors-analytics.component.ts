import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
@Component({
  selector: 'app-instructors-analytics',
  templateUrl: './instructors-analytics.component.html',
  styleUrls: ['./instructors-analytics.component.scss']
})
export class InstructorsAnalyticsComponent implements OnInit {

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
  }

  tiles: any = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({matches}) => {
      if(matches){
        return [
          {text: 'One', cols: 2, rows: 1, color: 'lightblue'},
          {text: 'Two', cols: 2, rows: 1, color: 'lightgreen'},
          {text: 'Three', cols: 2, rows: 1, color: 'lightpink'},
          {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
         
        ];
        console.log(matches);     
      }
      return [
        {text: 'One', cols: 1, rows: 1, color: 'lightblue'},
        {text: 'Two', cols: 1, rows: 1, color: 'lightgreen'},
        {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
        {text: 'Four', cols: 1, rows: 1, color: '#DDBDF1'},
       
      ];
    })
  ) 

}
