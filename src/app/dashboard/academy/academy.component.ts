import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-academy',
  templateUrl: './academy.component.html',
  styleUrls: ['./academy.component.scss']
})
export class AcademyComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
    console.log(`${Math.floor(Math.random()*6)*10}%`);
  }
 public iscompleted: Number=Math.floor(Math.random()*6)
  coursesObject:any = [
    {
      courseIndex: 0,
      course: 'web',
      courseTitle: `javascript basic`,
      courseDesc: `Beginner course for javascript and its basics`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 1,
      course: 'web',
      courseTitle: `Basics of Angular`,
      courseDesc: `Introductory course for Angular and framework basics`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 2,
      course: 'Android',
      courseTitle: `Android N: Quick Settings`,
      courseDesc: `Step by step guide for Android N: Quick Settings`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 3,
      course: 'firebase',
      courseTitle: `Google Assistant App`,
      courseDesc: `Dive deep into Google Assistant apps using Firebase`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 4,
      course: 'andriod',
      courseTitle: `Saving Data `,
      courseDesc: `Learn how to keep your important data safe and private`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 5,
      course: 'cloud',
      courseTitle: `Foundry App's Using Apigee Edge`,
      courseDesc: `Introductory course for Pivotal Cloud Foundry App`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 6,
      course: 'web',
      courseTitle: `Build a PWA Using Workbox`,
      courseDesc: `Step by step guide for building a PWA using Workbox`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 7,
      course: 'firebase',
      courseTitle: `Cloud Functions for Firebase`,
      courseDesc: `Beginners guide of Firebase Cloud Functions and Fundamentals`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 8,
      course: 'cloud',
      courseTitle: `Building a gRPC Service with Java`,
      courseDesc: `Learn more about building a gRPC Service with Java`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    },
    {
      courseIndex: 9,
      course: 'andriod',
      courseTitle: ` App with Firebase `,
      courseDesc: `Dive deep into User Management on iOS apps using Firebase`,
      courseTiming: `${Math.floor(Math.random()*200)} minutes`,
      cousrseCompleted: Math.floor(Math.random()*6) ,     
    }

  ]
  
  

}
