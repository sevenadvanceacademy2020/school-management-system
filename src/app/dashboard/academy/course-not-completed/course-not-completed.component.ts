import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-course-not-completed',
  templateUrl: './course-not-completed.component.html',
  styleUrls: ['./course-not-completed.component.scss']
})
export class CourseNotCompletedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
