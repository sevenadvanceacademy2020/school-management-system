import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseNotCompletedComponent } from './course-not-completed.component';

describe('CourseNotCompletedComponent', () => {
  let component: CourseNotCompletedComponent;
  let fixture: ComponentFixture<CourseNotCompletedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseNotCompletedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseNotCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
