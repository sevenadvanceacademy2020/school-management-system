import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  settings:boolean = true;
  team:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  meetTeam(){
    this.team = true;
    this.settings = false;
  }
  setting(){
    this.team = false;
    this.settings = true;
  }

}
