import { DialogComponent } from './shared/dialog/dialog.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AcademyComponent } from './academy/academy.component';
import { CourseCompletedComponent } from './academy/course-completed/course-completed.component';
import { CourseNotCompletedComponent } from './academy/course-not-completed/course-not-completed.component';
import { ChatComponent } from './chat/chat.component';
import { UsersComponent } from './chat/users/users.component';
import { ChatBotComponent } from './chat/chat-bot/chat-bot.component';
import { ContactComponent } from './contact/contact.component';
import { StudentContactComponent } from './contact/student-contact/student-contact.component';
import { InstructorContactComponent } from './contact/instructor-contact/instructor-contact.component';
import { EditContactComponent } from './contact/edit-contact/edit-contact.component';
import { SidebarContactComponent } from './contact/sidebar-contact/sidebar-contact.component';
import {ClipboardModule} from '@angular/cdk/clipboard';
import { AdminDasboardComponent } from './scrumboard/admin-dasboard/admin-dasboard.component';
import { WeeklyPlanningComponent } from './scrumboard/weekly-planning/weekly-planning.component';
import { TaskReviewComponent } from './scrumboard/task-review/task-review.component';
import { MoreReviewComponent } from './scrumboard/more-review/more-review.component';
import { GeneralComponent } from './task/general/general.component';
import { AddTaskComponent } from './task/add-task/add-task.component';
import { SideBarTaskComponent } from './task/side-bar-task/side-bar-task.component';
import { NotesComponent } from './notes/notes.component';
import { StudentsNotesComponent } from './notes/students-notes/students-notes.component';
import { InstructorsNotesComponent } from './notes/instructors-notes/instructors-notes.component';
import { TaskNotesComponent } from './notes/task-notes/task-notes.component';
import { DialogBoxComponent } from './notes/dialog-box/dialog-box.component';
import { HomeComponent } from './help/home/home.component';
import { GuideComponent } from './help/guide/guide.component';
import { SupportsComponent } from './help/supports/supports.component';
import { SchoolDataComponent } from './fileManager/school-data/school-data.component';
import { DetailBarComponent } from './fileManager/detail-bar/detail-bar.component';
import { AccountSettingsComponent } from './settings/account-settings/account-settings.component';
import { AdministrativeComponent } from './settings/administrative/administrative.component';
import { NotificationComponent } from './settings/notification/notification.component';
import { DailyComponent } from './activities/daily/daily.component';
import { MonthlyComponent } from './activities/monthly/monthly.component';
import { YearlyComponent } from './activities/yearly/yearly.component';
import { StudentsComponent } from './students/students.component';
import { DetailsComponent } from './students/details/details.component';
import { StudentsAnalyticsComponent } from './general/students-analytics/students-analytics.component';
import { InstructorsAnalyticsComponent } from './general/instructors-analytics/instructors-analytics.component';
import { GeneralHomeComponent } from "./general/generalHome.component";
import { SchoolsAnalyticsComponent } from './general/schools-analytics/schools-analytics.component';
import { ProfileComponent } from './profile/profile.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatBadgeModule} from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { InstructorsComponent } from './instructors/instructors.component';
import { RightComponent } from './general/right/right.component';
import { LeftComponent } from './general/left/left.component';
import { GridComponent } from './general/grid/grid.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { ScheduleComponent } from './general/schedule/schedule.component';
import { TaskReviewsComponent } from './general/task-reviews/task-reviews.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatInputModule } from "@angular/material/input";
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSelectModule} from '@angular/material/select'
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { TestingFormComponent } from './help/testing-form/testing-form.component';
import { MatRadioModule } from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
// import { MatNativeDateModule } from '@angular/material/native-date';
// import {MatNativeDateModule} from '@angular/material';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatMomentDateModule } from "@angular/material-moment-adapter";
@NgModule({
  declarations: [
    DashboardComponent,
    AcademyComponent,
    GeneralHomeComponent,
    CourseCompletedComponent,
    CourseNotCompletedComponent,
    ChatComponent,
    UsersComponent,
    ChatBotComponent,
    ContactComponent,
    StudentContactComponent,
    InstructorContactComponent,
    EditContactComponent,
    SidebarContactComponent,
    AdminDasboardComponent,
    WeeklyPlanningComponent,
    TaskReviewComponent,
    MoreReviewComponent,
    GeneralComponent,
    AddTaskComponent,
    SideBarTaskComponent,
    NotesComponent,
    StudentsNotesComponent,
    InstructorsNotesComponent,
    TaskNotesComponent,
    DialogBoxComponent,
    HomeComponent,
    GuideComponent,
    SupportsComponent,
    SchoolDataComponent,
    DetailBarComponent,
    AccountSettingsComponent,
    AdministrativeComponent,
    NotificationComponent,
    DailyComponent,
    MonthlyComponent,
    YearlyComponent,
    StudentsComponent,
    DetailsComponent,
    StudentsAnalyticsComponent,
    InstructorsAnalyticsComponent,
    SchoolsAnalyticsComponent,
    ProfileComponent,
    InstructorsComponent,
    RightComponent,
    LeftComponent,
    GridComponent,
    ScheduleComponent,
    TaskReviewsComponent,
    DialogComponent,
    TestingFormComponent,
    // MatMomentDateModule,
  ],
  imports: [
    MatNativeDateModule, 
    CommonModule,
    MatDatepickerModule,
    // MatNativeDateModule,
    DashboardRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatDialogModule,
    MatCardModule,
    MatMenuModule,
    MatBadgeModule,
    MatExpansionModule,
    MatDividerModule,
    FormsModule,
    ReactiveFormsModule,
    HighchartsChartModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatProgressBarModule,
    MatSelectModule,
    MatCheckboxModule,
    ClipboardModule,
    MatSnackBarModule,
    MatRadioModule
  ]
})
export class DashboardModule { }
