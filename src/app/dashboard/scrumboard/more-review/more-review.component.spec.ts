import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreReviewComponent } from './more-review.component';

describe('MoreReviewComponent', () => {
  let component: MoreReviewComponent;
  let fixture: ComponentFixture<MoreReviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoreReviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
