import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { GeneralHomeComponent } from './general/generalHome.component';

// academy
import { AcademyComponent } from './academy/academy.component';
import { CourseCompletedComponent } from './academy/course-completed/course-completed.component';
import { CourseNotCompletedComponent } from './academy/course-not-completed/course-not-completed.component';

// chat
import { ChatComponent } from './chat/chat.component';
import { UsersComponent } from './chat/users/users.component';
import { ChatBotComponent } from './chat/chat-bot/chat-bot.component';

// contact
import { ContactComponent } from './contact/contact.component';
import { StudentContactComponent } from './contact/student-contact/student-contact.component';
import { InstructorContactComponent } from './contact/instructor-contact/instructor-contact.component';
import { EditContactComponent } from './contact/edit-contact/edit-contact.component';
import { SidebarContactComponent } from './contact/sidebar-contact/sidebar-contact.component';

// scrumboard
import { AdminDasboardComponent } from './scrumboard/admin-dasboard/admin-dasboard.component';
import { WeeklyPlanningComponent } from './scrumboard/weekly-planning/weekly-planning.component';
import { TaskReviewComponent } from './scrumboard/task-review/task-review.component';
import { MoreReviewComponent } from './scrumboard/more-review/more-review.component';

// task
import { GeneralComponent } from './task/general/general.component';
import { AddTaskComponent } from './task/add-task/add-task.component';
import { SideBarTaskComponent } from './task/side-bar-task/side-bar-task.component';

// notes
import { NotesComponent } from './notes/notes.component';
import { StudentsNotesComponent } from './notes/students-notes/students-notes.component';
import { InstructorsNotesComponent } from './notes/instructors-notes/instructors-notes.component';
import { TaskNotesComponent } from './notes/task-notes/task-notes.component';
import { DialogBoxComponent } from './notes/dialog-box/dialog-box.component';

// help
import { HomeComponent } from './help/home/home.component';
import { GuideComponent } from './help/guide/guide.component';
import { SupportsComponent } from './help/supports/supports.component';

// file manager
import { SchoolDataComponent } from './fileManager/school-data/school-data.component';
import { DetailBarComponent } from './fileManager/detail-bar/detail-bar.component';

// settings
import { AccountSettingsComponent } from './settings/account-settings/account-settings.component';
import { AdministrativeComponent } from './settings/administrative/administrative.component';
import { NotificationComponent } from './settings/notification/notification.component';

// activities
import { DailyComponent } from './activities/daily/daily.component';
import { MonthlyComponent } from './activities/monthly/monthly.component';
import { YearlyComponent } from './activities/yearly/yearly.component';

// students
import { StudentsComponent } from './students/students.component';
import { DetailsComponent } from './students/details/details.component';

// general
import { StudentsAnalyticsComponent } from './general/students-analytics/students-analytics.component';
import { InstructorsAnalyticsComponent } from './general/instructors-analytics/instructors-analytics.component';

import { SchoolsAnalyticsComponent } from './general/schools-analytics/schools-analytics.component';
// profile
import { ProfileComponent } from './profile/profile.component';
import { InstructorsComponent } from './instructors/instructors.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: GeneralHomeComponent,
        children: [
          {
            path: '',
            component: SchoolsAnalyticsComponent,
          },
          {
            path: 'instructorsAnalytics',
            component: InstructorsAnalyticsComponent,
          },
          {
            path: 'studentsAnalytics',
            component: StudentsAnalyticsComponent,
          },
        ],
      },
      {
        path: 'students',
        component: StudentsComponent,
        children: [{ path: 'details', component: DetailsComponent }],
      },
      {
        path: 'academy',
        component: AcademyComponent,

        children: [
          { path: 'completed', component: CourseCompletedComponent },
          {
            path: 'notcompleted',
            component: CourseNotCompletedComponent,
          },
        ],
      },
      {
        path: 'chat',
        component: ChatComponent,
        children: [
          { path: 'user', component: UsersComponent },
          { path: 'chat-bot', component: ChatBotComponent },
        ],
      },
      {
        path: 'contact',
        component: ContactComponent,
        children: [
          // EditContactComponent,SidebarContactComponent
          { path: 'students', component: StudentContactComponent },
          { path: 'instructors', component: InstructorContactComponent },
        ],
      },
      {path: 'profile', component: ProfileComponent},
      {
        path: 'scrumboard',
        component: AdminDasboardComponent,
        children: [
          { path: 'weekly', component: WeeklyPlanningComponent },
          { path: 'task', component: TaskReviewComponent },
          { path: 'more', component: MoreReviewComponent },
        ],
      },
      {
        path: 'task',
        component: GeneralComponent,
        children: [
          { path: 'add', component: AddTaskComponent },
          // SideBarTaskComponent
        ],
      },
      {path: 'credits',component: StudentsNotesComponent },
      { path: 'schoolarship', component: InstructorsNotesComponent },
      { path: 'attendance', component: TaskNotesComponent },
      {
        path: 'projects',
        component: NotesComponent,
      },
      { path: 'supports', component: SupportsComponent },
      { path: 'guide', component: GuideComponent },
      {
        path: 'help',
        component: HomeComponent,
      },
      {
        path: 'filemanager',
        component: SchoolDataComponent,
        children: [{ path: 'details', component: DetailBarComponent }],
      },
      {
        path: 'settings',
        component: AccountSettingsComponent,
        children: [
          { path: 'administrative', component: AdministrativeComponent },
          { path: 'notification', component: NotificationComponent },
        ],
      },
      {
        path: 'activity',
        component: DailyComponent,
        children: [
          { path: 'month', component: MonthlyComponent },
          { path: 'year', component: YearlyComponent },
        ],
      },
      {
        path: 'instructors',
        component: InstructorsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
