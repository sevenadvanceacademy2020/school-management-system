import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.scss']
})
export class DailyComponent implements OnInit {
  createActivity:boolean = false;
  time:string = new Date().toString().split(' ').splice(0, 5).join(' ')
  dateConstructor:string = new Date().getHours() <12?this.time+' '+'AM':this.time+' '+'PM'
  constructor() { }

  ngOnInit(): void {
  }
  toggleActivityForm(){
    this.createActivity = !this.createActivity;
  }

}
