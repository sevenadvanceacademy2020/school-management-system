import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarTaskComponent } from './side-bar-task.component';

describe('SideBarTaskComponent', () => {
  let component: SideBarTaskComponent;
  let fixture: ComponentFixture<SideBarTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideBarTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
