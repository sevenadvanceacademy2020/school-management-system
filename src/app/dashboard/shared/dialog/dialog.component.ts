import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA,MatDialog, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
export interface DialogData {
  peerId?: string;
  joinCall: string;
}
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  constructor(
    public dialogRef:MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData, private _snackBar:MatSnackBar

  ) { }

  ngOnInit(): void {
  }

  /**
   * showCopiedSnackBar
   */
  public showCopiedSnackBar() {
    this._snackBar.open('peer ID Copied!', 'Hurray', {
      duration:1000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
