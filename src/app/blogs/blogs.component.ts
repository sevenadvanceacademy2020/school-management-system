import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../shared/services/contentful.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss']
})
export class BlogsComponent implements OnInit {

  blogs!: Entry<any>[];

  constructor(public contentFulService:ContentfulService) { }

  ngOnInit(): void {
    this.getBlogs()
  }

  getBlogs() {
    this.contentFulService.getBlogs()
    .then((blogs) => {
      this.blogs = blogs
      console.log(this.blogs)
    })
  }

}
