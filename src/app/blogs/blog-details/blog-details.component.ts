import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentfulService } from 'src/app/shared/services/contentful.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss']
})
export class BlogDetailsComponent implements OnInit {

  blog:any;

  constructor(private route: ActivatedRoute, public contentfulService:ContentfulService) { }

  ngOnInit(): void {
    this.getBlog()
  }

  getBlog() {
    const id = this.route.snapshot.paramMap.get('id');
    this.contentfulService.getBlog(id).then((blog) => {
      this.blog = blog
    })
  }

}
