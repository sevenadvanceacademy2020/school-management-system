import { Component, HostBinding, HostListener, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ width: '0', opacity: 0 }),
        animate('0.5s', style({ opacity: 1, width:'50%' })),
      ]),
      transition(':leave', [
        animate('0.3s', style({ width: '0', opacity: 0 }))
      ])
    ]),
  ]
})
export class HeaderComponent implements OnInit {

  isFixedNavbar = false;
  isOpen = false;
  logged!: boolean;


  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getLogin()
  }

  ngDoCheck() {
    this.getLogin()
  }

  getLogin() {
    this.logged = this.authService.logged
  }

  logout() {
    this.authService.logout();
  }

  toggleMenu() {
    this.isOpen = !this.isOpen;
  }
}
