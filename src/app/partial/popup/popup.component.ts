import { Component, OnInit } from '@angular/core';
import { PopupService } from '../../shared/services/popup.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  showPopup:boolean = true;
  popupMessage:string = "";
  errorMessage:boolean = true;

  constructor(private popupService:PopupService) { }

  ngOnInit(): void {
  }

  ngDoCheck() {
    this.showPopup = this.popupService.showPopup
    this.popupMessage = this.popupService.popupMessage
    this.errorMessage = this.popupService.errorMessage
  }

  closePopup() {
    this.showPopup = false
    this.popupService.showPopup = this.showPopup
  }

}
