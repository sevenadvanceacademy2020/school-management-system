import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';
import { StudentGuard } from './shared/guards/student.guard';
import { InstructorGuard } from './shared/guards/instructor.guard';
import { AdminGuard } from './shared/guards/admin.guard';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [

  { path: 'home', component: LandingComponent },
  {
    path: 'admin',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then((m) => m.DashboardModule),

  },

  {
    path: 'auth',
    loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },

  { path: 'schools', loadChildren: () => import('./schools/schools.module').then(m => m.SchoolsModule) },

  { path: 'about', loadChildren: () => import('./about/about.module').then(m => m.AboutModule) },

  { path: 'blogs', loadChildren: () => import('./blogs/blogs.module').then(m => m.BlogsModule) },

  { path: 'contact', loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule) },

  { path: 'user-profile', loadChildren: () => import('./user-profile/user-profile.module').then(m => m.UserProfileModule), /**canActivate: [AuthGuard]*/ },

  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
