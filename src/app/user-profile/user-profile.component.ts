import { Component, OnInit } from '@angular/core';
import { User } from '../shared/interfaces/user';
import { AuthService } from '../shared/services/auth.service';
import { UsersService } from '../shared/services/users.service';
import { CourseService } from '../shared/services/course.service';
import { Course } from '../shared/interfaces/course';
import { PopupService } from '../shared/services/popup.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user!: User;
  editable: boolean = false;
  userCourses!: Course[];
  photoURL: string = ""
  changePhotoIn: boolean = false;

  constructor(
    public authService: AuthService,
    private userService: UsersService,
    public courseService: CourseService,
    private popupService: PopupService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit(): void {
    this.getUser()
  }

  getUser() {
    this.user = this.authService.user
    this.getUserCourses()
  }

  editProfile() {
    this.editable = true
  }

  closeEdit() {
    this.editable = false
  }

  save() {
    this.userService.saveUser(this.user).then(() => {
      this.popupService.successPopup("your profile has been successfully edited")
      this.editable = false
    })
  }

  getUserCourses() {
    this.courseService.getUserCourses(this.user.email).subscribe((courses) => {
      this.userCourses = courses
    })
  }

  changePhoto() {
    this.changePhotoIn = true
  }


  savePicture() {
    if (this.photoURL == "") {
      this.popupService.failurePopup("you need to import an image")
      this.changePhotoIn = false
    } else {
      this.user.photoURL = this.photoURL
      this.userService.saveUser(this.user).then(() => {
        this.popupService.successPopup("your profile picture has been successfully edited")
        this.changePhotoIn = false
      })
    }
  }


  uploadPicture(event: any) {
    const file = event.target.files[0];
    const filePath = 'users/usersProfile/' + this.user.uid;
    const fileRef = this.storage.ref(filePath);
    // uploads the chosen image to the file reference
    const task = this.storage.upload(filePath, file);
    // observe percentage changes
    // this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe(downloadURL => {
          //  sets the image url to the download url gotten
          this.photoURL = downloadURL;
          //  console.log(downloadURL);
        })
      })
    )
      .subscribe()
  }
}
